# EndToEnd_WCL_Cases_Matlab

This repo comes with various Matlab scripts each employs different architecture-based analysis:

1- analysis_1.mlx: This script applies Pipeline theorem when there is global round robin scheme used in the memory system.\
2- analysis_2.mlx: This script employs the same theorem but with separate round robin in LLC and DRAM.\
3- analysis_3.mlx: In analysis 3, there's no round robin among the all stages and the bound is computed based on an additive latency approach.\
4- pipeline_theorem_all_cases.mlx: This script, covers all possible cases regarding request under analysis using the method in analysis_1. It's implemented based on a brute force approach.\ 

## Getting started

Here the steps to run "pipeline_theorem_all_cases.mlx" are explained:\

1- The two first code sections are used to put all timing parameters such as DDR4 timing constraints, clock period into Matlab worspace. These values will be used in subsequent sections.\
2- Based on the paper, there are different types of request each has its own execution stages. Therefore, the number of types of interfering request can varry. The 3rd and 4th sections are used to generate different number of interfering requests for each type.\
3- In following sections, each sections is annotated wth specific type of a request and the analysis is applied for different patterns generated in step 2.\

Likewise, the user can follow the same steps for other scripts.\